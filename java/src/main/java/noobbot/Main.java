package noobbot;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    Piece[] pieces;
    long timePrevious;
    long timeNow;
    Position myPreviousPosition;
    LaneForTrack[] trackLanes;
    double previousForce;
    double previousThrottle;
    double frictionForce;
    double previousSpeed;
    double previousAcceleration;
    double limitSpeedFactor;
    //double kFactor;
    //ListArray<Node> nodes;
    int lastIndexLaneChanged;
    double maxAngle;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        
        this.timePrevious = 0;
        this.timeNow = 0;
        this.myPreviousPosition = null;
        this.trackLanes = null;
        this.previousForce = 0;
        this.previousThrottle = 0;
        this.frictionForce = 26.0;
        this.previousSpeed = 0;
        this.previousAcceleration = 0;
        this.limitSpeedFactor = 0.018;
        //this.kFactor = 3479;
        //this.nodes = new ArrayList<Node>();
        this.lastIndexLaneChanged = -1;
        this.maxAngle = 0;
        
        send(join);
        //send(new CreateRace(new BotId(join.name, join.key), "usa", "qwerty", 1));

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            //System.out.println(line);
            if (msgFromServer.msgType.equals("carPositions")) {
            	this.timePrevious = this.timeNow;
            	this.timeNow = System.currentTimeMillis();
            	controlCar(line, join.name);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                //System.out.println(msgFromServer.data);
                initTrack(line);
            	System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("ULKONA!");
                this.frictionForce = this.frictionForce * 0.9;
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
	
    private void controlCar(String data, String botName) {
    	
    	//String dataString = data.toString();  	
  	    Position[] positions;
        PositionData pd = gson.fromJson(data, PositionData.class);
        //positions = gson.fromJson(dataString, Position[].class);
    	positions = pd.data;
        
    	// My car's position now
    	Position myPositionNow = null;
    	for (Position p : positions) {
    		if (p.id.name.equals(botName)) {
    			myPositionNow = p;
    			break;
    		}
    	}
		int myPieceIndex = myPositionNow.piecePosition.pieceIndex;		
        
    	double throttle = 0.0;
    	double force = 0;
    	double mySpeed = this.getSpeed(myPositionNow, this.myPreviousPosition);
    	double myAcceleration = (mySpeed-this.previousSpeed) / (this.timeNow - this.timePrevious);
    	double angleLimitBeforeFullAhead = 20;
    	
    	// suora
    	int pieceCount = this.pieces.length;
    	if (this.pieces[myPieceIndex].length > 1 && this.pieces[(myPieceIndex+1)%pieceCount].length > 1) { 
    		throttle = 1.0;
    		
    	// lahestyvä mutka	
    	} else if (this.pieces[myPieceIndex].length > 1 && this.pieces[(myPieceIndex+1)%pieceCount].radius > 1) { 
    		double targetSpeed = Math.sqrt(this.frictionForce / 10000 * (this.pieces[(myPieceIndex+1)%pieceCount].radius - 
    				this.trackLanes[myPositionNow.piecePosition.lane.endLaneIndex].distanceFromCenter*this.pieces[(myPieceIndex+1)%pieceCount].angle / Math.abs(this.pieces[(myPieceIndex+1)%pieceCount].angle)));
    		this.maxAngle = 0;
            if (myPositionNow.piecePosition.inPieceDistance/this.pieces[myPieceIndex].length > 0.0 && 
        			this.angleLeftInCurve(myPositionNow.piecePosition.lane.endLaneIndex, (myPieceIndex+1)%pieceCount, 0) > angleLimitBeforeFullAhead) {  			
    			if (mySpeed > targetSpeed) {
        			throttle = 0;
        			//System.out.print("(T:" + this.previousThrottle + ", A:" + myAcceleration + ", S:" + mySpeed + ")");
        		} else {
        			throttle = targetSpeed / this.limitSpeedFactor;
        		}
    		} else {
    			throttle = this.previousThrottle;
    		} 
    		
    	// mutkassa
    	} else {
    		force = mySpeed*mySpeed / (this.pieces[myPieceIndex].radius - this.trackLanes[myPositionNow.piecePosition.lane.endLaneIndex].distanceFromCenter * this.pieces[(myPieceIndex)%pieceCount].angle / Math.abs(this.pieces[(myPieceIndex)%pieceCount].angle)) * 10000;
    		double thisRadius;
    		if (this.pieces[(myPieceIndex+1)%this.pieces.length].radius > 0 && this.pieces[myPieceIndex].radius > this.pieces[(myPieceIndex+1)%this.pieces.length].radius) {
    			thisRadius = (this.pieces[(myPieceIndex+1)%this.pieces.length].radius - this.trackLanes[myPositionNow.piecePosition.lane.endLaneIndex].distanceFromCenter * this.pieces[(myPieceIndex)%pieceCount].angle / Math.abs(this.pieces[(myPieceIndex)%pieceCount].angle));
    		} else {
    			thisRadius = (this.pieces[myPieceIndex].radius - this.trackLanes[myPositionNow.piecePosition.lane.endLaneIndex].distanceFromCenter * this.pieces[(myPieceIndex)%pieceCount].angle / Math.abs(this.pieces[(myPieceIndex)%pieceCount].angle));
    		}
    				
    		double targetSpeed = Math.sqrt(this.frictionForce / 10000 * thisRadius);
            
            if (myPositionNow.angle > this.maxAngle) {
                this.maxAngle = myPositionNow.angle;
            }
    		
    		//mutka loppuu, suora alkaa, kiihdytetaan
    		if (this.pieces[(myPieceIndex+1)%pieceCount].length > 0 && this.angleLeftInCurve(myPositionNow.piecePosition.lane.endLaneIndex, myPieceIndex, myPositionNow.piecePosition.inPieceDistance) < angleLimitBeforeFullAhead) {
    			throttle = 1.0;
                if (this.maxAngle < 10) {
                   this.frictionForce = this.frictionForce * 1.05;
                   this.maxAngle = 90;
                }
    		//liian kova vauhti
    		} else if (mySpeed > targetSpeed) {
    			throttle = 0;
    		//haetaan nopeus oikeaksi	
    		} else {
    			throttle = targetSpeed / this.limitSpeedFactor;
    		}
    		/*if (force > 0)
    			System.out.println(force + " " + throttle);*/
    		
    	}	
    	
    	double maxThrottle = 1.0;
    	if (throttle > maxThrottle) {
    		throttle = maxThrottle;
    	}
    		
    	this.previousThrottle = throttle;
    	this.previousForce = force;
    	this.myPreviousPosition = myPositionNow;
    	this.previousSpeed = mySpeed;
    	this.previousAcceleration = myAcceleration;
    	
    	//System.out.print("(T:" + throttle + ", F:" + force + ") ");
    	//System.out.print("(T:" + throttle + ", A:" + myAcceleration + ", S:" + mySpeed + ")");
    	
    	//System.out.println(this.pieces[myPieceIndex].length + " " + this.pieces[myPieceIndex].Switch);
    	if(this.pieces[(myPieceIndex+1)%this.pieces.length].Switch && this.lastIndexLaneChanged != myPieceIndex) {
    		int bestLaneIndex=0;
    		double minDist = Double.MAX_VALUE;
    		for (int lane=0; lane<this.trackLanes.length; lane++) {
    			if (Math.abs(myPositionNow.piecePosition.lane.startLaneIndex-lane) <= 1) {
    				double dist = this.getDistance(myPieceIndex, lane); 
    				//System.out.println(dist + " " + lane);
    				if (dist <= minDist) {
    					minDist = dist;
    					bestLaneIndex = lane;
    				}
    			}
    		}
    		if (bestLaneIndex-myPositionNow.piecePosition.lane.startLaneIndex < 0) {
    			send(new SwitchLane("Left"));
    			//System.out.println("Left");
    		} else if (bestLaneIndex-myPositionNow.piecePosition.lane.startLaneIndex > 0) {
    			send(new SwitchLane("Right"));
    			//System.out.println("Right");
    		} else {
    			send(new Throttle(throttle));
    		}
    		this.lastIndexLaneChanged = myPieceIndex;
    	} else {
    		send(new Throttle(throttle));
    	}
    	
    }
    
    
    private void initTrack(String data) {
    	
         //try {
         //String dataString = data.toString();    	
    	 //String dataString = data;
         //System.out.println(dataString);
         data = data.replaceAll("switch", "Switch");
         final Data d = gson.fromJson(data, Data.class);
         //final RaceSettings innerData = gson.fromJson(dataString, RaceSettings.class);
    	 /*dataString = innerData.getData().toString();
    	 final Race race = gson.fromJson(dataString, Race.class);
    	 dataString = race.getTrack().toString();
    	 final Track track = gson.fromJson(dataString, Track.class);
    	 dataString = track.getPieces().toString();*/
    	 //dataString = dataString.replaceAll("switch", "Switch");
    	 this.pieces = d.data.race.track.pieces;
         //this.pieces = gson.fromJson(dataString, Piece[].class);
    	 //dataString = track.lanes.toString();
    	 this.trackLanes = d.data.race.track.lanes;
         //this.trackLanes = gson.fromJson(dataString, LaneForTrack[].class);
        //} catch (Exception e) {
        //  System.out.println("Poikkeus: initTrack ei onnistunut!");
        //}
    	
    	
    	
    }
    
    // "getDistanceToNextSwitchPiece"
    private double getDistance(int pieceIndex, int lane) {
    	double distance = 0;
    	int index = (pieceIndex+2)%this.pieces.length;
    	// laskee risteyspalasta eteenpain matkan
    	while (!this.pieces[index].Switch) {
    		if (this.pieces[index].length > 0) {
    			distance += this.pieces[index].length;
    		}
    		else {
    			distance += this.curveLength(lane, (index));
    		}
    		index = (index+1)%this.pieces.length;
    	}
    	// lasketaan viela risteyspalan matkan mukaan
    	if (this.pieces[index].length > 0) {
			distance += this.pieces[index].length;
		}
		else {
			distance += this.curveLength(lane, index);
		}
    	return distance;
    }
    
    /*private int getMinDistance() {
    	double min = Double.MAX_VALUE;
    	int minIndex = -1;
    	for(int i=0; i<this.nodes.size(); i++) {
    		if (this.nodes.get(i).distance < min) {
    			minIndex = i;
    			min = this.nodes.get(i).distance;
    		}
    	}
    	return minIndex;
    }*/
    
    private double getSpeed(Position now, Position previous) {
    	double speed = -1;
    	if (previous != null) {
    		if (now.piecePosition.pieceIndex != previous.piecePosition.pieceIndex) { //vaihdettu ratapalaa
    			if (this.pieces[previous.piecePosition.pieceIndex].length > 1) {  // edellinen pala suora
    				speed = now.piecePosition.inPieceDistance + this.pieces[previous.piecePosition.pieceIndex].length-previous.piecePosition.inPieceDistance;
    			}
    			else {			//edellinen pala kaarre		
    				speed = now.piecePosition.inPieceDistance + this.curveLength(previous.piecePosition.lane.endLaneIndex, previous.piecePosition.pieceIndex) -previous.piecePosition.inPieceDistance;
    			}
    		}
    		else { //pysytty samalla ratapalalla
    			speed = now.piecePosition.inPieceDistance - previous.piecePosition.inPieceDistance;
    		}
    		speed = speed / (this.timeNow-this.timePrevious);
    	}
    	return speed;
    }
    
    
    private double curveLength(int laneIndex, int pieceIndex) {
    	double length = 0;
    	Piece piece = this.pieces[pieceIndex];
    	LaneForTrack lane = this.trackLanes[laneIndex];
    	
    	if (piece.angle * lane.distanceFromCenter > 0) { 	//sisakaarteessa
    		length = (piece.radius - Math.abs(lane.distanceFromCenter)) * Math.PI * 2 * Math.abs(piece.angle) / 360; 
    		//System.out.println("Sisakaarre!");
    	} else { 											//ulkokaarteessa
    		length = (piece.radius + Math.abs(lane.distanceFromCenter)) * Math.PI * 2 * Math.abs(piece.angle) / 360;
    		//System.out.println("Ulkokaarre!");
    	} 	
    	return length;
    }
    
    private double angleLeftInCurve(int laneIndex, int pieceIndex, double inPieceDistance) {
    	double angle = 0;  	
    	angle += (1 - inPieceDistance / this.curveLength(laneIndex, pieceIndex)) * Math.abs(this.pieces[pieceIndex].angle); 	
    	//System.out.print(angle + " " + pieceIndex + " ");
    	int index = 1;
    	while (this.pieces[pieceIndex].angle * this.pieces[(pieceIndex+index)%this.pieces.length].angle > 0) {
    		angle += Math.abs(this.pieces[(pieceIndex+index)%this.pieces.length].angle);
    		index += 1;
    	}
    	//System.out.println(angle + " " + pieceIndex);
    	return angle;
    }

}

/*------------------*/

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

/*------------------*/

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

/*------------------*/

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

/*------------------*/

class CreateRace extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;
    

    CreateRace(final BotId botid, final String trackName, final String password, final int carCount) {
        this.botId = botid;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

/*------------------*/

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

/*------------------*/

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

}

/*------------------*/

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

}

/*------------------*/
class PositionData {
    public final String msgType;
    public final Position[] data;
    public final String gameId;
    
    public PositionData(final String msgType, final Position[] data, final String gameId) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
    }
}

/*------------------*/

class RaceSettings {
    public final Race race;
	
	public RaceSettings(final Race race) {
		this.race = race;
	}
	
	public Race getData() {
		return this.race;
	}
}

/*------------------*/
class Data {
    public final String msgType;
    public final RaceSettings data;
    
    public Data(final String msgType, final RaceSettings data) {
        this.msgType = msgType;
        this.data = data;
    }
}

/*------------------*/

class Race {
    public final Track track;
    public final Object cars;
    public final Object raceSession;

    public Race(final Track track, final Object cars, final Object raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
    
    /*public Race(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }*/
    
    public Track getTrack() {
    	return this.track;
    }
    
    public Object getCars() {
    	return this.cars;
    }
    
    public Object getRaceSession() {
    	return this.raceSession;
    }
}

/*------------------*/

class Track {
    public final String id;
    public final String name;
    public final Piece[] pieces;
    public final LaneForTrack[] lanes;
    public final Object startingPoint;

    public Track(final String id, final String name, final Piece[] pieces, final LaneForTrack[] lanes, final Object startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }
    
    /*public Track(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }*/
    
    public String getId() {
    	return this.id;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public Piece[] getPieces() {
    	return this.pieces;
    }
    
    public LaneForTrack[] getLanes() {
    	return this.lanes;
    }
    
    public Object getStartingPoint() {
    	return this.startingPoint;
    }
}

/*------------------*/

class Piece {
	public final double length;
	public final double radius;
	public final double angle;
	public final boolean Switch;
	
	public Piece(final double length) {
		this.length = length;
		this.radius = -1;
		this.angle = -1;
		this.Switch = false;
	}
	
	public Piece(final double length, final boolean Switch) {
		this.length = length;
		this.radius = -1;
		this.angle = -1;
		this.Switch = Switch;
	}
	
	public Piece(final double radius, final double angle) {
		this.length = -1;
		this.radius = radius;
		this.angle = angle;
		this.Switch = false;
	}
	
	public Piece(final double radius, final double angle, final boolean Switch) {
		this.length = -1;
		this.radius = radius;
		this.angle = angle;
		this.Switch = Switch;
	}
	
}

/*------------------*/

class Lane {
	public final int startLaneIndex;
	public final int endLaneIndex;
	
	public Lane(final int start, final int end) {
		this.startLaneIndex = start;
		this.endLaneIndex = end;
	}
}

/*------------------*/

class PiecePosition {
	public final int pieceIndex;
    public final double inPieceDistance;
    public final Lane lane;
    public final int lap;
    
    public PiecePosition(final int pieceIndex, final double inPieceDistance, final Lane lane, final int lap) {
    	this.pieceIndex = pieceIndex;
    	this.inPieceDistance = inPieceDistance;
    	this.lane = lane;
    	this.lap = lap;
    }
}

/*------------------*/

class ID {
	public final String name;
	public final String color;
	
	public ID(final String name, final String color) {
		this.name = name;
		this.color = color;
	}
}

/*------------------*/

class Position {
	public final ID id;
	public final double angle;
	public final PiecePosition piecePosition;
	
	public Position(final ID id, final double angle, final PiecePosition pp) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = pp;
	}
}

/*------------------*/
class LaneForTrack {
	public final int distanceFromCenter;
	public final int index;
	
	public LaneForTrack(final int distance, final int index) {
		this.distanceFromCenter = distance;
		this.index = index;
	}
}

/*------------------*/

class BotId {
	public final String name;
	public final String key;
	
	public BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}

